`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.03.2020 23:28:14
// Design Name: 
// Module Name: datamemory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DataMemory(
    input clock,
    input reset,
    input [63:0] offst,
    input [6:0] opcode,
    input [2:0] state,
    input [63:0] stype_op,
    output reg [63:0] data
    );
    
    reg [63:0] M [0:100];
    integer data_f;
    integer scan_f;
    integer i,fd;
    reg [63:0] stored_data;
    
    `define NULL 0 
    initial 
    begin
        
          data_f = $fopen("memory.txt", "r");
          if (data_f == `NULL) begin
            $finish;
          end
          
        for(i = 0;i<=99;i = i + 1)
            begin
                scan_f = $fscanf(data_f, "%b", stored_data);
                M[i] = stored_data;            
            end
    
    end
    
    
    
always@(posedge clock)
begin
    if(state == 3'b100)
    begin
            if(opcode == 7'b0000011)
            begin
                
               data = M[offst];
                         
            end
  

     if(opcode == 7'b0100011)
    begin
    
       
       M[offst] = stype_op;
       
                fd = $fopen ("memory.txt", "w");
                for (i = 0; i <= 99; i = i + 1) begin
                
               
                  $fwrite (fd,"%b\n" ,M[i]);
                end
                  $fclose(fd);
                  
        
    
    end
    
     
  end

 end    
endmodule

