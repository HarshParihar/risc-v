`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.03.2020 23:27:11
// Design Name: 
// Module Name: registerfile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterFile(
    input clock,
    input reset,
    input [4:0] rs1,
    input [4:0] rs2,
    input [4:0] rd,
    output reg [63:0] op1,
    output reg [63:0] op2,
    output [63:0] final,
    input [2:0] state,
    input [63:0] data_out,
    input [31:0] jal_data,
    input [6:0] opcode
    );
    
    reg [63:0] stored_data; 
    integer i,j,dest,tmp,data_f,scan_f,fd,k;    
    reg [63:0] Y [0:31];  
    
    
    
`define NULL 0    
initial begin
         data_f = $fopen("regFile.txt", "r");
          if (data_f == `NULL) begin
            $finish;
          end
          
        for(tmp = 0;tmp<=31;tmp = tmp + 1)
            begin
                scan_f = $fscanf(data_f, "%b", stored_data);
                Y[tmp] = stored_data;
            
            end
      
end
    
   
always@(posedge clock)
    begin
    
       
       if(state == 3'b101)
       begin
       
                if(opcode == 7'b0000011)
                begin
                    
                    Y[rd] = data_out;
                
                end
                else if(opcode == 7'b1101111)
                begin
                    
                    Y[rd] = jal_data;
                
                end
                else if(opcode == 7'b0010011)
                begin
                    Y[dest] = final;    
                end
                
                else if(opcode == 7'b0110011)
                begin
                    Y[dest] = final;    
                end
       
                fd = $fopen ("regFile.txt", "w");
                for (i = 0; i <= 31; i = i + 1) 
                begin
             
                  $fwrite (fd,"%b\n" ,Y[i]);
                end
                  $fclose(fd);
                  
            
        end
      
    
    
        if(state == 3'b011)
        begin
    
                i = rs1;
                j = rs2;
                dest = rd;    
                op1 = Y[i];
                op2 = Y[j];
                
                     
        end
       
    end
endmodule
