`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.03.2020 23:22:32
// Design Name: 
// Module Name: IM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionMemory(
    input clock,
    input [31:0] pc,
    input reset,
    input [2:0] state,
    output reg [31:0] out
    );
    
    reg [31:0] R [0:31];
    integer data_f;
    integer scan_f;
    integer i,fd;
    reg [31:0] stored_data;
   
    `define NULL 0
    initial
    begin
       
          data_f = $fopen("Instruction.txt", "r");
          if (data_f == `NULL) begin
            $finish;
          end
         
        for(i = 0;i<=32;i = i + 1)
            begin
                scan_f = $fscanf(data_f, "%b", stored_data);
                R[i] = stored_data;            
            end
   
    end
    
    always@(posedge clock)
        begin    
     
               if(state == 3'b001)
               begin
                    out = R[pc];
               end
        end            
endmodule

