`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.03.2020 23:31:00
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module ALU(
    input clock,
    input reset,
    input [63:0] a,
    input [63:0] b,
    input [31:0] pc,
    input [6:0] funct7,opcode,
    input [2:0] funct3,
    input [4:0] rs2,rs1,rd,
    output reg [63:0] out,
    output reg [63:0] offst,
    output reg [31:0] offst1,
    output reg [31:0] out1
    );
    
    reg [11:0] tmp;
    reg [63:0] imm;
    reg [5:0] shamt; 

always@(posedge clock)
begin
      offst1 = 32'd1;
  //R - Type
      if(opcode == 7'b0110011)
        begin
            if(funct7 == 7'b0)
            begin
                //ADD
                if(funct3 == 3'b0)
                begin
                 out = a+b;
                end
                //AND
                if(funct3 == 3'b111)
                begin
                    out = a & b;
                end
                //OR
                if(funct3 == 3'b110)
                begin
                
                    out = a | b;
                end
                //XOR
                if(funct3 == 3'b100)
                begin
                
                    out = a ^ b;
                end
                //Shift Left L
                if(funct3 == 3'b001)
                begin
                    out = a << b;
                end
                //Shift Right L
                if(funct3 == 3'b101)
                begin
                    out = a >> b;
                end 
            end
    
           else
               //Sub
               if(funct3 == 3'b0)
                begin
                     out = a-b;
                end
                //SRA
                if(funct3 == 3'b101)
                begin
                    out = $signed(a) >>> b;
                end
        end
    
        //I - Type 
        if(opcode == 7'b0010011)
        begin
            if(funct3 == 3'b0)
            begin
                tmp = {funct7,rs2};
                imm = $signed(tmp);
                out = imm + a;
            end
            //Not Operation
            if(funct3 == 3'b100)
            begin
                out = a ^ 16'hFFFFFFFFFFFFFFFF;
            end
            //Shift Left LI
            if(funct3 == 3'b001)
            begin
                tmp = {funct7,rs2};
                shamt = tmp[5:0];
                out = a << shamt;
            end
            //Shift Right 
             if(funct3 == 3'b101)
            begin
                tmp = {funct7,rs2};
                shamt = tmp[5:0];
                //LI
                if(tmp[11:6] == 6'b010000)
                begin    
                    out = a >> shamt;
                end
                //AI
                else
                begin
                    out = a >>> shamt;
                end
            end
        end
        
        //I - Type Load
        if(opcode == 7'b0000011)
        begin       
            tmp = {funct7,rs2};
            imm = {{53{tmp[11]}}, tmp};
            offst = a + imm;
        end
        
        //S-Type
        if(opcode == 7'b0100011)
        begin
            tmp = {funct7,rd};
            imm = {{53{tmp[11]}},tmp};
            offst = a + imm;
            out = b;
        end    
        //Branch instructions
        if(opcode == 7'b1100111)
        begin
            imm = {funct7[6],rd[0],funct7[5:0],rd[4:1]};
                if(funct3 == 3'b000)
                begin 
                    if(a == b)
                    begin 
                        offst1 = $signed(imm);
                    end
                end
                if(funct3 == 3'b001)
                begin 
                    if(a != b)
                    begin 
                        offst1 = $signed(imm);        
                    end
                end
                if(funct3 == 3'b100)
                begin 
                    if(a < b)
                    begin 
                        offst1 = $signed(imm); 
                    end
                end
                if(funct3 == 3'b101)
                begin 
                    if(a >= b)
                    begin 
                        offst1 = $signed(imm);
                    end
                end
        end
        //Jump instructions
        //jal
        if(opcode == 7'b1101111)
        begin
            imm = { funct7[6],rs1,funct3,rs2[0],funct7[5:0],rs2[4:1] };
            offst1 = $signed(imm);
            out1 = pc + 1;
        end
        //jalr
        if(opcode == 7'b1101111 && funct3 == 3'b001)
        begin
            imm = {funct7,rs2};
            offst1 = a + $signed(imm) - pc;
        end
    end
 
endmodule