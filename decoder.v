`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.03.2020 23:23:12
// Design Name: 
// Module Name: decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Decoder(
      input clock,
      input reset
    );
    
     wire [4:0] rs1,rs2,rd;
     wire [6:0] funct7,opcode;
     wire [2:0] funct3;    
     wire [63:0] op1,op2,final;
     wire [31:0] out;
     wire [31:0] jal_data;
     wire [31:0] out1;
     wire [63:0] offst;
     wire [31:0] offst1;
     reg [2:0] state = 3'b001;
     
     wire [63:0] data_out;
     reg [31:0] pc = 32'b0;
     
        InstructionMemory insmem(clock,pc,reset,state,out);                
        Main main(out,clock,reset,rs1,rs2,rd,funct7,opcode,funct3,state);       
        RegisterFile regf(clock,reset,rs1,rs2,rd,op1,op2,final,state,data_out,out1,opcode);
        DataMemory dm(clock,reset,offst,opcode,state,final,data_out);
        ALU alu(clock,reset,op1,op2,pc,funct7,opcode,funct3,rs2,rs1,rd,final,offst,offst1,out1);
      
        
  always@(posedge clock)
    begin       
        
        case(state)
            
            3'b001 : state = 3'b010;
            3'b010 : state = 3'b011;
            3'b011 : state = 3'b100;
            3'b100 : state = 3'b101;
            3'b101 : 
                begin
                pc = pc + offst1;
                state = 3'b001;           
                end
        endcase
  
    end 
endmodule

