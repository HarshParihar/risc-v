`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2020 22:05:29
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module tb();
    reg clk;
    reg reset;
    initial begin
    clk=0;
    forever
    #5 clk=~clk;
    end
    initial begin
    reset=0;
    #5 reset=1;
    end
    Decoder DUT(clk,reset);
endmodule
